#! /usr/bin/env ruby
# :copyright: Copyright 2013 by Jay HOTTA.
# :license: Apache License 2.0

# referenced bash script
# for i in `grep po ../../progress.md | awk '{print $12}'`; do echo $i; msgfmt --statistics $i; done

require "./pot2po"

if __FILE__ == $0

	dir = "../locale/ja/LC_MESSAGES"
	extent_in = ".po"

	pot_files = PoManipulater.new(extent_in, dir)
	pot_files.list.each { |element|
		msgfmt = "msgfmt -v --statistics #{element}"
		system(msgfmt)
  }

end