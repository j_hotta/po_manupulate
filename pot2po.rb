#! /usr/bin/env ruby
# :copyright: Copyright 2012 by Jay HOTTA.
# :license: Apache License 2.0

require "fileutils"

class PoManipulater

	def initialize (extention=".pot", search_base=".")
		@search_file = "*" + extention
		@search_base = search_base

	end

	def list
		items = Dir.glob(File.join(@search_base, @search_file))
		return items
	end

	def check4_dir(dir)
		if ! FileTest.exists?(dir)
			FileUtils.mkdir_p(dir)
		end
	end

end


if __FILE__ == $0

	pot_dir = "../translate"
	extent_in = ".pot"
	extent_out = ".po"
	folder_path = "/ja/LC_MESSAGES"
	po_tmp = "../po_tmp"
	locale_folder = File.join("../locale", folder_path)

	pot_files = PoManipulater.new(extent_in, pot_dir)

	# chef if target dir exist?
	pot_files.check4_dir(po_tmp)
	pot_files.check4_dir(locale_folder)

	pot_files.list.each { |element|
		input_file = File.join(pot_dir, element)
		file_base = File.basename(element, extent_in)
		output_file = File.join(po_tmp, file_base) + extent_out
		locale_output_file = File.join(locale_folder, file_base) + extent_out
		
		# msginit
		msginit = "msginit --no-translator --locale=ja --input=#{input_file} --output=#{output_file}" 
		system(msginit)

		# msgmerge
		if FileTest.exist?(locale_output_file)
			p "merging #{input_file} to #{locale_output_file}"
			msgmerge = "msgmerge -U #{locale_output_file} #{input_file}"
			system(msgmerge)
		else
			org = File.join(po_tmp, file_base) + extent_out
			FileUtils.copy(org, locale_folder)
		end
	}

end
