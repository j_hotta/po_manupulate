#! /usr/bin/env ruby
# :copyright: Copyright 2012 by Jay HOTTA.
# :license: Apache License 2.0

require "./pot2po"

if __FILE__ == $0

	dir = "../locale/ja/LC_MESSAGES"
	extent_in = ".po"
	extent_out = ".mo"

	pot_files = PoManipulater.new(extent_in, dir)
	pot_files.list.each { |element|
		file_base = File.basename(element, extent_in)
		output_file = File.join(dir, file_base) + extent_out
		msgfmt = "msgfmt #{element} -o #{output_file}"
		system(msgfmt)
  }

end